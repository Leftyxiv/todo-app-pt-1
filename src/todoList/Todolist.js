import React from "react";
import TodoItem from "../todoItem/Todoitem";

function TodosList(props) {
  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos.map((todo) => (
          <TodoItem
            title={todo.title}
            completed={todo.completed}
            id={todo.id}
            handleToggle={props.handleToggle}
            key={todo.id}
            handleDeleteTodo={() => props.handleDeleteTodo(todo.id)}
          />
        ))}
      </ul>
    </section>
  );
}

export default TodosList;
