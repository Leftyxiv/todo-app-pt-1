import React, { useState } from "react";
import { todos as todoList } from "./todos";
import { v4 as uuid } from "uuid";
import TodoItem from "./todoItem/Todoitem";
import TodosList from "./todoList/Todolist";

function App(props) {
  const [todos, setTodos] = useState(todoList);
  const [inputText, setInputText] = useState("");
  const [counter, updateCounter] = useState(4);

  const handleAddToDo = (event) => {
    if (event.which === 13) {
      const newId = uuid();
      const newTodo = {
        userId: newId,
        id: newId,
        title: inputText,
        completed: false,
      };
      const newTodos = { ...todos };
      newTodos[newId] = newTodo;
      setTodos(newTodos);
      setInputText("");
      updateCounter(counter + 1);
    }
  };

  const handleToggle = (id) => {
    const newTodos = { ...todos };
    if (newTodos[id].completed) {
      updateCounter(counter + 1);
    } else {
      updateCounter(counter - 1);
    }
    newTodos[id].completed = !newTodos[id].completed;
    setTodos(newTodos);
  };
  const handleDeleteTodo = (id) => {
    let newTodos = { ...todos };
    if (!newTodos[id].completed) {
      updateCounter(counter - 1);
    }
    delete newTodos[id];
    setTodos(newTodos);
  };
  const handleDeleteAll = () => {
    const newTodos = { ...todos };
    for (const todo in newTodos) {
      if (newTodos[todo].completed) {
        delete newTodos[todo];
      }
    }
    setTodos(newTodos);
  };
  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          onChange={(event) => setInputText(event.target.value)}
          onKeyDown={(event) => handleAddToDo(event)}
          value={inputText}
          className="new-todo"
          placeholder="What needs to be done?"
          autoFocus
        />
      </header>
      <TodosList todos={Object.values(todos)} handleToggle={handleToggle} handleDeleteTodo={handleDeleteTodo} />
      <footer className="footer">
        <span className="todo-count">
          <strong>{counter}</strong> item(s) left
        </span>
        <button className="clear-completed" onClick={handleDeleteAll}>
          Clear completed
        </button>
      </footer>
    </section>
  );
}

export default App;
